import java.util.*;
public class OXNEW {

	public static void main(String[] args) {
		Scanner kb = new Scanner (System.in);
		char[][]board = {{' ','1','2','3'},{'1','-','-','-'},{'2','-','-','-'},{'3','-','-','-'}};
		char Player = 'X';
		String result; 
		showWelecome();
		for(;;) {
			showBoard(board);
			showturn(Player);
			input(board,Player);
			if(CheckWin(board,Player))break;
			Player = ChangeTurn(Player);	
		}
	}

	public static void showWelecome() {
		System.out.println("Welecome to OX Game");
	}
	public static void showBoard(char[][]board) {
		for(int i = 0 ; i < 4;i++) {
			for(int j = 0 ; j < 4;j++) {
				System.out.print(board[i][j]+" ");
			}System.out.println();
		}
		
	}
	public static void showturn(char Player) {
		System.out.println(Player+" turn");
	}
	public static void input(char [][]board , char Player) {
		Scanner kb = new Scanner(System.in);
		int row;
		int col;
		for(;;) {
			for(;;) {
				System.out.print("Please input Row Col : ");
				row=kb.nextInt();
				col=kb.nextInt();
				if(row < 1 || row > 3) {
					System.out.println("Error to input try again");
					continue;
				}else if(col < 1 || col > 3)	{
					System.out.println("Error to input try again");
					continue;
				}else {
					break;
				}
			}
			if(board[row][col] != '-' ) {
				System.out.println("Error to input try again");
			}else {
				board[row][col] = Player;
				break;
			}
		}
		
	}
	public static boolean CheckWin(char [][]board,char Player) {
		int numberPlayer=0;
		if(board[1][1] == Player&&board[1][2] == Player&&board[1][3] == Player ||
				   board[1][1] == Player&&board[1][2] == Player&&board[1][3] == Player || 
				   board[1][1] == Player&&board[1][2] == Player&&board[1][3] == Player) {
					if (Player == 'X') numberPlayer=1 ;
					else numberPlayer=2;
					showBoard(board);
					System.out.println("Congratuiation Player "+numberPlayer+" YOU ARE THE WINNER");
					return true;
					
				}else if(board[1][1] == Player&&board[2][1] == Player&&board[3][1] == Player ||
						 board[1][2] == Player&&board[2][2] == Player&&board[3][2] == Player || 
						 board[1][3] == Player&&board[2][3] == Player&&board[3][3] == Player) {
					if (Player == 'X') numberPlayer=1 ;
					else numberPlayer=2;
					showBoard(board);
					System.out.println("Congratuiation Player "+numberPlayer+" YOU ARE THE WINNER");
					return true;
				}else if(board[1][1] == Player&&board[2][2] == Player&&board[3][3] == Player ||
						 board[1][3] == Player&&board[2][2] == Player&&board[3][1] == Player) {
					if (Player == 'X') numberPlayer=1 ;
					else numberPlayer=2;
					showBoard(board);
					System.out.println("Congratuiation Player "+numberPlayer+" YOU ARE THE WINNER");
					return true;
					
				}else if( board[1][1] != '-' &&board[1][2] != '-' &&board[1][3] != '-' &&
						  board[2][1] != '-' &&board[2][2] != '-' &&board[2][3] != '-' &&
						  board[3][1] != '-' &&board[3][2] != '-' &&board[3][3] != '-' ){
					if (Player == 'X') numberPlayer=1 ;
					else numberPlayer=2;
					showBoard(board);
					System.out.println("Draw");
					return true;
					
				}
		return false;
	}
	public static char ChangeTurn(char Player) {
		if(Player == 'X' ) {
			return 'O';
		}return 'X';
		
	}
	
}